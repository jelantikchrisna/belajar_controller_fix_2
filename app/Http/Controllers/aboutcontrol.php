<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\aboutcon;

class aboutcontrol extends Controller
{
    public function index()
    {
        return view('about',[
            "title" => "About",
            "aboutcon" => aboutcon::all()
        ]);
    }
}
