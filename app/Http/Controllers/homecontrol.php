<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\content;

class homecontrol extends Controller
{
    public function index()
    {
        return view('home',[
            "title" => "Home",
            "content" => content::all()
        ]);
    }
}
