<?php

use App\Http\Controllers\aboutcontrol;
use App\Http\Controllers\homecontrol;
use App\Http\Controllers\portocontrol;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', [homecontrol::class, 'index']);

Route::get('/portofolio', [portocontrol::class, 'index']);

Route::get('/about', [aboutcontrol::class, 'index']);

