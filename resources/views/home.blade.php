@extends('layout')

{{-- content home --}}
@section('content')
   <div class="jumbotron text-center shadow-sm">
       <img src="img/profil.jpg" class="rounded-circle" >
       <h1>Chrisna Bukian</h1>
       <p>UI/UX design | Photgrapher</p>
   </div>

{{-- welcome --}}
<div class="welcome shadow-sm">
    <div class="container text-center">
        <div class="row">
            <div class="col-sm-12">
                <h1>Welcome To My Portofolio</h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p>{{ $content["welcome"] }}

                </p>
            </div>
        </div>
    </div>
</div>
{{-- end welcome --}}

{{-- prestasi --}}
<div class="prestasi shadow-sm">
    <div class="container">
        <div class="row ">
            <div class="col-sm-4 tex-center">
                <img src="img/ui.png" class=" rounded-circle shadow  bg-white" alt="ui" style="width: 300px; height: 300px; margin-left: 40px; margin-top: 40px;margin-bottom: 20px;">
            </div>
            <div class="col-sm-8">
                <h1>UI & UX Design</h1>
                <hr>
                <p>{{ $content["ui"] }} <a href="/portofolio" class="badge badge-info"> Clik Disini</a>
                </p>
            </div>
        </div>
    </div>
</div>
{{-- end prestasi --}}

{{-- Photographi --}}
<div class="photographi">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h1>Photographi</h1>
                <hr>
                <p>{{ $content["photograpi"] }}<a href="/portofolio" class="badge badge-info"> Clik Disini</a></p>
            </div>
            <div class="col-sm-4">
                <img src="img/photo.jpeg" class="rounded-circle shadow" alt="photo" style="width: 300px; height: 300px; margin-right: 40px; margin-top: 50px;margin-bottom: 20px;">
            </div>
        </div>
    </div>
</div>
{{-- endphotographi --}}
@endsection
{{-- end content home  --}}