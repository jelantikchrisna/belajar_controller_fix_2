@extends('layout')

@section('content')

<div class="jumbotron text-center shadow-sm">
    <img src="img/profil.jpg" class="rounded-circle" >
    <h1>Chrisna Bukian</h1>
    <p>UI/UX design | Photgrapher</p>
</div>

<div class="prt shadow-sm">
    <div class="container text-center">
        <div class="row">
            <div class="col-sm-12">
                <h1>UI & UX Design</h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <a href="">
                    <img src="img/1ui.png" alt=""  class="img-thumbnail" style="width: 250px; height:250px;" >
                </a>
                <h4>PT. Persero</h4>
            </div>
            <div class="col-sm-4">
                <a href="">
                    <img src="img/2ui.jpg" alt=""  class="img-thumbnail" style="width: 250px; height:250px;" >
                </a>
                <h4>PT. Benika</h4>
            </div>
            <div class="col-sm-4">
                <a href="">
                    <img src="img/3ui.jpg" alt=""  class="img-thumbnail" style="width: 250px; height:250px;" >
                </a>
                <h4>PT. Consul IT</h4>
            </div>
        </div>
    </div>
</div>
<div class="photo shadow-sm">
    <div class="container text-center">
        <div class="row">
            <div class="col-sm-12">
                <h1>Photographi</h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <a href="">
                    <img src="img/1ph.jpg" alt=""  class="img-thumbnail" style="width: 250px; height:250px;" >
                </a>
                <h4>Souce</h4>
            </div>
            <div class="col-sm-4">
                <a href="">
                    <img src="img/2ph.jpg" alt=""  class="img-thumbnail" style="width: 250px; height:250px;" >
                </a>
                <h4>Watch</h4>
            </div>
            <div class="col-sm-4">
                <a href="">
                    <img src="img/3ph.jpg" alt=""  class="img-thumbnail" style="width: 250px; height:250px;" >
                </a>
                <h4>Fashion</h4>
            </div>
        </div>
    </div>
</div>

@endsection