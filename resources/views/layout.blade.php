<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>My Profile | {{ $title }}</title>
  </head>
  <body>

    {{-- nav --}}
    <div class="container">
    <nav class="navbar navbar-expand-lg navbar-dark bg-secondary" style="margin-top: 10px;" >
        <a class="navbar-brand" href="#">Profil Digital</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item {{ ($title === "Home") ? 'active' : '' }}">
              <a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item {{ ($title === "Portofolio") ? 'active' : '' }}">
              <a class="nav-link" href="/portofolio">Portofolio</a>
            </li>
            <li class="nav-item {{ ($title === "About") ? 'active' : '' }}">
              <a class="nav-link" href="/about">About</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
    {{-- akhir nav --}}

    {{-- content --}}
    <div class="container">
        @yield('content')
    </div>
    <footer>
        <div class="container">
            <div class="row">
                <p>copright 2021 | build by. <a href="https://www.facebook.com/krisnametalblenks/" style="color:turquoise;" ">Chrisna</a> with Bootstrap</p>
            </div>
        </div>
    </footer>
    {{-- end content --}}
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>