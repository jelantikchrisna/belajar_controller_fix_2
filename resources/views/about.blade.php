@extends('layout')

@section('content')
<div class="About">
    <div class="container">
        <div class="row ">
            <div class="col-sm-4 tex-center">
                <img src="img/profil.jpg" class=" rounded-circle shadow  bg-white" alt="ui" style="width: 300px; height: 300px; margin-left: 40px; margin-top: 40px;margin-bottom: 20px;">
            </div>
            <div class="col-sm-8">
                <h1>My Profil</h1>
                <hr>
                <p>{{ $aboutcon["aboutcon"] }}</p>
            </div>
        </div>
    </div>
</div>
<div class="contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1>Contact</h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8 offset-sm-2">
                <form>
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" id="nama" class="form-control" placeholder="Masukan Nama">
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" id="alamat" class="form-control" placeholder="Masukan Alamat">
                    </div>
                    <div class="form-group">
                        <label for="hp">No.Hp</label>
                        <input type="tel" id="hp" class="form-control" placeholder="Masukan No.Hp">
                    </div>
                    <div class="form-group">
                        <label for="pesan">Category</label>
                        <select class="form-control" id="pesan">
                            <option>UI & UX Design</option>
                            <option>Photography</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="pesan">Pesan</label>
                        <Textarea class="form-control" rows="10" id="pesan" placeholder="Masukan Pesan"></Textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection